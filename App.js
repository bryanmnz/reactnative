/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 * @flow
 */

import React, { Component } from 'react';
import {
  Platform,
  StyleSheet,
  Text,
  TouchableHighlight,
  View
} from 'react-native';
import {
  StackNavigator,
} from 'react-navigation';

const Login = require("./src/components/loginView")
const Dashboard = require("./src/components/dashboardView")
const DetailSite = require("./src/components/siteDetailView")
const Nav = StackNavigator({
    Dashboard: { 
      screen: Dashboard, 
      path:'./src/components/dashboardView'    
    },
    DetailSite: { 
      screen: DetailSite, 
      path:'./src/components/siteDetailView'    
    }
});  
export default class App extends Component<{}> {
  render() {    
    return (
      <Nav></Nav>      
    );
  }
}

const styles = StyleSheet.create({

});
