
'use strict'
import React, { Component } from 'react';
import {
	View,
	Text,
	Image,
	TouchableHighlight,
	Alert,
	StyleSheet
} from 'react-native'

class loginView extends Component{
	render(){
		return(
			<Image style={styles.container} source={{uri:'https://images.pexels.com/photos/247932/pexels-photo-247932.jpeg?w=940&h=650&auto=compress&cs=tinysrgb'}}>
				<View>
					<Text style={styles.title}>Lista de Sitios</Text>
					<TouchableHighlight onPress={(this.onLogin.bind(this))} style={styles.boton}>
						<Text style={styles.textoBoton}>Ingresar</Text>
					</TouchableHighlight>
				</View>
			</Image>
		);
	}

	onLogin(){
		Alert.alert("Acceso",
			"Welcome",
			[
				{
					text:'Aceptar',
					onPress:(this.aceptar.bind(this))
				},
				{
					text:'Cancelar',
					onPress:(this.cancelar.bind(this))
				}
			]
			);
		console.log("Se pulso Login");
	}

	aceptar(){
		this.props.navigation.navigate('Dashboard', { user: 'Lucy' });	
	}
	cancelar(){
		
	}
}

const styles = StyleSheet.create({
	boton:{
		width:300,
		height:30,
		backgroundColor:'red',
		alignItems:'center',
		marginTop:60,
		marginBottom:10,
		borderRadius:8,
		borderWidth:1
	},
	textoBoton: {
		color:'white'
	},
	container: {
	    flex: 1,
	    alignItems: 'stretch',
	    padding: 30
  	},
  	title: {
  		marginTop: 100,
  		fontSize: 25,
  		textAlign:'center',
  		backgroundColor:'rgba(0,0,0,0);'
  	}
});

module.exports = loginView;