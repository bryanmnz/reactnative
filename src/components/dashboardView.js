
'use strict'
import React, { Component } from 'react';
import {
	View,
	StyleSheet,
	ListView,RefreshControl,
	TouchableHighlight,
	Button,
	Image,
	Text
} from 'react-native'
const REQUEST_URL = "https://app.distribucionesjr.com.co/api/Site/Get";

class dashboardView extends Component{


	constructor(){		
		super();
		this.state={
			dataSource:new ListView.DataSource({
				rowHasChanged: (row1,row2)=>row1 != row2
			}),
			loaded: false,refreshing: false,
		}
	}
	static navigationOptions = ({ navigation }) =>({
	    title: `Sitios`
	    
	});

	componentDidMount(){		
		this.fetchData();
	}

	fetchData(){
		fetch(REQUEST_URL)
		.then((response)=>response.json())
		.then((responsedata)=> {
			console.log(responsedata);
			this.setState({
			dataSource: this.state.dataSource.cloneWithRows(responsedata.data),
			loaded:true,refreshing: false
		})
		}).catch((error) => {
        console.error(error);
      });
	}

	reloadListView(){
		this.fetchData();
	}

	renderLoadingView(){
		return(
			<View style={styles.container}>
				<Text>Cargando Sitios..</Text>
			</View>);
	}

	_onRefresh() {
    this.setState({refreshing: true});
    fetch(REQUEST_URL)
		.then((response)=>response.json())
		.then((responsedata)=> {
			console.log(responsedata);
			this.setState({
			dataSource: this.state.dataSource.cloneWithRows(responsedata.data),
			refreshing: false
		})
		}).catch((error) => {
        console.error(error);
      });
  }

	renderSite(site){
		return(
			<TouchableHighlight onPress={()=>this.onSitePressed(site)}>
			<Image source={{uri:'https://app.distribucionesjr.com.co/images/'+site.ST_Image}} style={styles.backgroundImage}>
				<View style={styles.rightContainer}>
					<Text style={styles.title}>{site.ST_Name}</Text>
					<Text style={styles.address}>{site.ST_Address}</Text>
				</View>
			</Image>
			</TouchableHighlight>
			);
	}

	render(){		
		if(!this.state.loaded){
			return this.renderLoadingView();
		}
		return(
			<ListView dataSource={this.state.dataSource}
			refreshControl={
          <RefreshControl
            refreshing={this.state.refreshing}
            onRefresh={this._onRefresh.bind(this)}/>
        }
			renderRow={this.renderSite.bind(this)}
			style={styles.ListView}/>);
	}

	onSitePressed(site){
		this.props.navigation.navigate('DetailSite', site);
	}
}

const styles = StyleSheet.create({
	backgroundImage:{
		justifyContent: 'center',
		alignItems:'center',
		alignSelf:'stretch',
		height:150
	},
	rightContainer:{
		backgroundColor:'rgba(52,52,52,0.5)',
		alignSelf:'stretch',
		paddingTop:30,
		height:150
	},
	container: {
	    flexDirection: 'row',
	    justifyContent: 'center',
		alignItems:'center',
		backgroundColor:'#F5FCFF'
  	},
  	title:{
  		fontSize:27,
  		marginBottom:0,
  		textAlign:'center',
  		backgroundColor:'rgba(52,52,52,0)',
  		color:'#ffffff'

  	},
  	ListView:{
  		paddingTop:30,
  		marginBottom:49
  	},
  	address:{
  		fontSize:18,
  		textAlign:'center',
  		backgroundColor:'rgba(52,52,52,0)',
  		color:'#ffffff'
  	},
  	button:{
  		width:300,
  		height:40,
  		marginLeft:1,
  		marginRight:1,
  		backgroundColor:'blue',
  	}
});

module.exports = dashboardView;