'use strict'
import React, { Component } from 'react';
import {
	View,
	Text,
	Image,
	TouchableHighlight,
	Alert,
	StyleSheet
} from 'react-native'

class siteDetailView extends Component{

	static navigationOptions = ({ navigation }) => ({
	    title: `${navigation.state.params.ST_Name}`,
	});

	constructor(){
		super();
	}
	render(){
		const { params } = this.props.navigation.state;
		this.passProps = params;
		return(
			<View style={styles.container}>
			<Image source={{uri:'https://app.distribucionesjr.com.co/images/'+this.passProps.ST_Image}} style={styles.image}/>

			<Text style={styles.title}>Sitio: {this.passProps.ST_Name}</Text>
			<Text style={styles.description}>Direccion: {this.passProps.ST_Address}</Text>
			<Text style={styles.description}>Administrador: {this.passProps.ST_NameAdministrator}</Text>
			</View>);
	}
}

const styles = StyleSheet.create({	
	container: {
	    marginTop:63,
	    flex:1,
	    backgroundColor:'#F5FCFF'
  	},
  	title:{
  		fontSize:23,
  		color:'#007AFF'
  	},
  	description:{
  		marginTop:10,
  		fontSize:16
  	},
  	image:{
  		alignSelf:'stretch',
  		height:300
  	}
});
module.exports = siteDetailView;